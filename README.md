![win 95 flag](Resources/flag.gif)

# Riq's Personal Website | V4 Retro 

Retro vibes personal website, not the best but it's working. little bit glitchy on mobile webview.

This is the repository for my public ~~[https://mikailthoriq.my.id](https://mikailthoriq.my.id)!~~

# Credits
This was made using a fork of [XP.css](https://github.com/AestheticalZ/XP.css), originally made by **botoxparty** (github), which is licensed under MIT.


![win 95 flag](Resources/flag.gif) ![nescape icon](Resources/NetGifs/netnow3.gif) ![nescape icon](Resources/NetGifs/iexplorer.gif)